Source: golang-github-brentp-irelate
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Nilesh Patra <nilesh@debian.org>
Section: devel
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-biogo-hts-dev,
               golang-gopkg-check.v1-dev,
               golang-github-pkg-errors-dev,
               golang-github-brentp-vcfgo-dev,
               golang-github-brentp-bix-dev,
               samtools
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-brentp-irelate
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-brentp-irelate.git
Homepage: https://github.com/brentp/irelate
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/brentp/irelate

Package: golang-github-brentp-irelate-dev
Architecture: all
Depends: golang-github-biogo-hts-dev,
         golang-gopkg-check.v1-dev,
         ${misc:Depends}
Description: Streaming relation testing of sorted files of intervals
 Helps to relate (e.g. intersect or by distance) sets of intervals.
 For example, if the nearest gene to a set of ChIP-Seq peaks needs
 to be reported, BEDTools does this extremely well, irelate is an
 attempt to provide an API so that users can write their own tools
 with little effort in go.
 .
 Performance irelate is quite fast, but use PIRelate for parallel
 intersection. It is less flexible than irelate, but skips parsing of
 database intervals for sparse regions in the query.  In addition, it
 has very good (automatic) parallelization.
